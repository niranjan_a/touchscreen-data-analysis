# README #

Install RStudio v 0.99.489 or the latest available version

Download Link: https://www.rstudio.com/products/rstudio/download/


Install the following packages:-

1. gdata
2. ggplot2
3. plyr
4. plotly

To install packages, run the following commands in RStudio console:-

install.packages("gdata")
install.packages("ggplot2")
install.packages("plyr")
install.packages("plotly")


Import the file "data_analysis1.R" in RStudio to check the plots

The graphs are plotted using ggplot2 and plotly packages.

The graphs plotted using plotly package are reactive, so they are made available in cloud. The following are the links to plots:-

	https://plot.ly/~niranjana88/50/pressure-plot-of-users-for-a-random-of-100-training-sets/

	https://plot.ly/~niranjana88/44/traces-of-user-id-36-in-various-applications/

	https://plot.ly/~niranjana88/30/touch-traces-of-user-id-36-in-nexus-1-experimenter-e/?share_key=NqeZl8zuXU74pk04nrYiIR